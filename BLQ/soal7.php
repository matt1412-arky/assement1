<?php

function hitungMean($deret)
{
    return array_sum($deret) / count($deret);
}

function hitungMedian($deret)
{
    sort($deret);
    $jumlah_data = count($deret);
    $index_tengah = floor($jumlah_data / 2);

    if ($jumlah_data % 2 == 0) {
        // Jumlah data genap
        return ($deret[$index_tengah - 1] + $deret[$index_tengah]) / 2;
    } else {
        // Jumlah data ganjil
        return $deret[$index_tengah];
    }
}

function hitungModus($deret)
{
    $frekuensi = array_count_values($deret);
    arsort($frekuensi);
    $modus_values = array_keys($frekuensi, max($frekuensi));
    return min($modus_values);
}

// Input deret angka dari pengguna
$input = readline("Masukkan deret angka (pisahkan dengan spasi): ");
$deret = array_map('intval', explode(' ', $input));

// Hitung statistik
$mean = hitungMean($deret);
$median = hitungMedian($deret);
$modus = hitungModus($deret);

// Menampilkan hasil
echo "Mean: $mean\n";
echo "Median: $median\n";
echo "Modus: $modus\n";
