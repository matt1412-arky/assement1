<?php

function hitungGunungDanLembah($perjalanan)
{
    $gunung = 0;
    $lembah = 0;
    $mdpl = 0; // Ketinggian saat ini

    for ($i = 0; $i < strlen($perjalanan); $i++) {
        $langkah = $perjalanan[$i];
        if ($langkah === 'N') {
            $mdpl++;
        } elseif ($langkah === 'T') {
            $mdpl--;
        }

        if ($langkah === 'T' && $mdpl === 0) {
            // Ini adalah lembah, karena turun dari 0 mdpl ke 0 mdpl
            $lembah++;
        } elseif ($langkah === 'N' && $mdpl === 0) {
            // Ini adalah gunung, karena naik dari 0 mdpl ke 0 mdpl
            $gunung++;
        }
    }

    return ['gunung' => $gunung, 'lembah' => $lembah];
}

$perjalanan = "N N T N N N T T T T T N T T T N T N";
$hasil = hitungGunungDanLembah($perjalanan);

echo "Hattori melewati " . $hasil['gunung'] . " gunung dan " . $hasil['lembah'] . " lembah";
