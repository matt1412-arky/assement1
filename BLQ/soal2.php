<?php
function calc_book_fine_per_day($day_borrowed, $day_returned)
{
    // Daftar buku beserta durasi peminjaman maksimum
    $book = array(
        'A' => 14,
        'B' => 3,
        'C' => 7,
        'D' => 7
    );

    // Besaran denda per hari
    $fine_per_day = 100;

    // Mengubah format tanggal menjadi objek DateTime
    $start_date = new DateTime($day_borrowed);
    $end_date = new DateTime($day_returned);

    // Menghitung selisih hari antara tanggal peminjaman dan pengembalian
    $interval = $start_date->diff($end_date);
    $borrowed_duration = $interval->days;

    // Inisialisasi array untuk menyimpan denda per buku
    $book_fines = [];

    // Menghitung denda untuk setiap buku
    foreach ($book as $key => $value) {
        $fined_days = $borrowed_duration - $value;
        if ($fined_days > 0) {
            // Menghitung denda
            $fined_amount = $fined_days * $fine_per_day;
        } else {
            $fined_amount = 0;
        }
        $book_fines[$key] = $fined_amount;
    };

    // Menampilkan hasil perhitungan denda untuk setiap buku
    echo "Denda untuk peminjaman buku dari tanggal $day_borrowed - $day_returned:\n";
    foreach ($book_fines as $key => $value) {
        if ($value > 0) {
            $value = 'Rp.' . number_format($value);
            echo "Buku $key denda $value\n";
        } else {
            echo "Buku $key tidak ada denda\n";
        }
    }
}

echo "Jawaban A:\n";
calc_book_fine_per_day('28-02-2016', '07-03-2016');
echo "\nJawaban B:\n";
calc_book_fine_per_day('29-4-2018', '30-5-2018');
