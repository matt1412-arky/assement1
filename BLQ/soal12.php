<?php

function bubbleSort(&$arr)
{
    $n = count($arr);

    for ($i = 0; $i < $n - 1; $i++) {
        for ($j = 0; $j < $n - $i - 1; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                // Tukar nilai jika urutan tidak sesuai
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
}

function tampilkanOutput($arr)
{
    echo implode(' ', $arr);
}

// Input dari pengguna
echo "Masukkan angka (pisahkan dengan spasi): ";
$inputString = trim(fgets(STDIN));

// Ubah string input menjadi array angka
$inputArray = array_map('intval', explode(' ', $inputString));

echo "Input: " . implode(' ', $inputArray) . "\n";

// Panggil fungsi bubbleSort untuk mengurutkan larik
bubbleSort($inputArray);

echo "Output: ";
tampilkanOutput($inputArray);
