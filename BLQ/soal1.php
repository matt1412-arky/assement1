<?php

// Jumlah uang awal yang dimiliki
$uang_awal = 1000;

// Daftar produk beserta harga-harga alternatifnya
$produk = array(
    array("nama" => "kacamata", 'harga' => array(500, 600, 700, 800)),
    array("nama" => "baju",      'harga' => array(200, 400, 350)),
    array("nama" => "sepatu",    'harga' => array(400, 350, 200, 300)),
    array("nama" => "buku",      'harga' => array(100, 50, 150))
);

// Fungsi untuk melakukan pembelian barang
function beli_barang($uang_awal, $produk)
{
    // Variabel untuk menyimpan jumlah uang yang tersisa, total harga barang yang dibeli, dan daftar barang yang dibeli
    $sisa_uang = $uang_awal;
    $total_barang = $uang_terpakai = 0;
    $daftar_barang = $daftar_harga = [];

    // Loop melalui setiap produk
    for ($i = 0; $i < count($produk); $i++) {
        // Periksa setiap harga dalam produk
        foreach ($produk[$i]['harga'] as $harga) {
            // Jika harga dapat dibeli
            if ($harga <= $sisa_uang) {
                // Simpan harga dan nama barang, kurangi uang tersisa, dan tambahkan harga ke uang yang terpakai
                $daftar_harga[] = $harga;
                $daftar_barang[] = $produk[$i]['nama'];
                $sisa_uang -= $harga;
                $uang_terpakai += $harga;
            }
        }
    }

    // Gabungkan barang dan harganya menjadi array asosiatif
    $barang_harga = array_combine($daftar_barang, $daftar_harga);

    // Hitung total barang yang dibeli
    $total_barang = count($daftar_barang);

    // Dapatkan barang terakhir yang dibeli
    $barang_terakhir = end($daftar_barang);

    // Output hasil pembelian
    echo "Jumlah uang awal: $uang_awal\n";
    echo "Total uang yang digunakan: $uang_terpakai\n";
    echo "Jumlah barang yang dapat dibeli: $total_barang (";
    foreach ($barang_harga as $barang => $harga) {
        echo "$barang $harga";
        if ($barang !== $barang_terakhir) {
            echo ", ";
        }
    }
    echo ")\n";
}

// Panggil fungsi untuk melakukan pembelian barang
beli_barang($uang_awal, $produk);
