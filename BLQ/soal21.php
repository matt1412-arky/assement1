<?php

$path = "_ _ _ _ _ O _ _ _";

function manhole($path)
{
    $path = explode(" ", $path);
    $steps = [];
    $st = 0;

    for ($i = 0; $i < count($path); $i++) {
        if (@$path[$i] == "O") {
            if ($st >= 2) {
                $st -= 2;
                $jumpCount = min(3, count($path) - $i - 1); // Jika sisa jarak kurang dari 3, ambil sisa jarak
                for ($j = 0; $j < $jumpCount; $j++) {
                    $steps[] = "J";
                    $i++;
                }
            } else {
                return "Failed";
            }
        } else {
            $st++;
            $steps[] = "W";
        }
    }

    $res = implode(" ", $steps);
    return $res;
}

echo manhole($path);
