<?php

function calculateParkingFee($entryDateTime, $exitDateTime)
{
    // Konversi tanggal dan waktu masuk dan keluar ke objek DateTime
    // Array nama bulan dalam bahasa Indonesia
    $monthNames = array(
        'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
        'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
    );

    // Ubah nama bulan dalam format tanggal ke bahasa Indonesia
    $entryDateTime = str_replace($monthNames, range(1, 12), $entryDateTime);
    $exitDateTime = str_replace($monthNames, range(1, 12), $exitDateTime);

    // Buat objek DateTime dari format yang sudah diubah
    $entryDateTime = DateTime::createFromFormat('d n Y H:i:s', $entryDateTime);
    $exitDateTime = DateTime::createFromFormat('d n Y H:i:s', $exitDateTime);

    // Periksa apakah format konversi berhasil
    if (!$entryDateTime || !$exitDateTime) {
        echo "Format tanggal dan waktu tidak valid!. Pastikan Anda memasukkan format yang benar.\n";
        return;
    }

    // Hitung selisih waktu dalam detik
    $timeDifference = $exitDateTime->getTimestamp() - $entryDateTime->getTimestamp();

    // Hitung selisih waktu dalam jam
    $hours = ceil($timeDifference / 3600);

    // Hitung tarif parkir berdasarkan ketentuan
    if ($hours <= 8) {
        // Kurang dari atau sama dengan 8 jam
        $parkingFee = $hours * 1000;
        $roundedHours = $hours;
        $feeExplanation = "dibulatkan menjadi $roundedHours jam mengacu pada ketentuan pertama, maka yang harus dibayarkan adalah $parkingFee rupiah.";
    } elseif ($hours <= 24) {
        // Lebih dari 8 jam, tetapi tidak lebih dari 24 jam
        $parkingFee = 8000;
        $roundedHours = $hours;
        $feeExplanation = "dibulatkan menjadi $roundedHours jam mengacu pada ketentuan kedua, maka yang harus dibayarkan adalah $parkingFee rupiah.";
    } elseif ($hours <= 32) {
        // Lebih dari 24 jam tetapi tidak lebih dari 32 jam
        $parkingFee = 15000;
        $additionalHours = $hours - 24;
        $parkingFee += $additionalHours * 1000;
        $feeExplanation = "dibulatkan menjadi $hours jam mengacu pada ketentuan ketiga, maka yang harus dibayarkan adalah $parkingFee rupiah.";
    } else {
        // Jika parkir lebih dari 32 jam
        echo "Parkir melebihi batas maksimum 32 jam.";
        return;
    }

    // Hitung selisih waktu dalam jam, menit, dan detik
    $timeDifference = $exitDateTime->getTimestamp() - $entryDateTime->getTimestamp();
    $hours = floor($timeDifference / 3600);
    $minutes = floor(($timeDifference % 3600) / 60);
    $seconds = $timeDifference % 60;

    echo "Penjelasan: Lamanya parkir adalah $hours jam $minutes menit $seconds detik, sehingga perhitungan tarif parkir $feeExplanation";
}

// Penggunaan/implentasi:
$entryDateTime = readline('Masukkan tanggal dan waktu masuk: ');
$exitDateTime = readline('Masukkan tanggal dan waktu keluar: ');

calculateParkingFee($entryDateTime, $exitDateTime);
