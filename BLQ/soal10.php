<?php

function rahasiakanKalimat($kalimat)
{
    $kata = explode(" ", $kalimat);
    $hasil = array();

    foreach ($kata as $k) {
        $panjangKata = strlen($k);
        if ($panjangKata > 2) {
            $k = $k[0] . '***' . $k[$panjangKata - 1];
        }
        $hasil[] = $k;
    }

    return implode(' ', $hasil);
}

// Meminta input kalimat dari pengguna
$kalimat = readline("Masukkan kalimat: ");

// Menyisipkan hanya 3 simbol '*' di tengah-tengah kalimat
$hasil = rahasiakanKalimat($kalimat);

// Menampilkan hasil
echo "Kalimat Asli: $kalimat\n";
echo "Kalimat Modifikasi: $hasil\n";
