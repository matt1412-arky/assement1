<?php

function tampilkanOutput($kata)
{
    $kata = strrev($kata);
    $panjang = strlen($kata);

    for ($i = 0; $i < $panjang; $i++) {
        for ($j = 0; $j < $panjang; $j++) {
            // Menggunakan mod untuk mengecek apakah panjang kata genap
            if ($panjang % 2 == 0) {
                // Jika genap, kata di tengah diapit oleh 3 simbol '*'
                if ($j == floor($panjang / 2)) {
                    echo $kata[$i] . "*";
                } else {
                    echo "*";
                }
            } else {
                // Jika ganjil, tampilkan seperti sebelumnya
                if ($j == floor($panjang / 2)) {
                    echo $kata[$i];
                } else {
                    echo "*";
                }
            }
        }
        echo "\n";
    }
}

// Penggunaan
$input = readline('Masukkan kata : ');
echo "Input: $input\n";
tampilkanOutput($input);
