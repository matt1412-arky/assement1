<?php

function convertTo24HourFormat($time12Hour)
{
    $time24Hour = date("H:i:s", strtotime($time12Hour));
    return $time24Hour;
}

// Input waktu dari pengguna
$time12Hour = readline("Masukkan waktu dalam format 12 jam (contoh: 03:40:44 PM): ");

// Memanggil fungsi untuk mengonversi waktu
$time24Hour = convertTo24HourFormat($time12Hour);

// Menampilkan hasil konversi
echo "Waktu dalam format 24 jam: $time24Hour\n";
