<?php

function hitungSudutJarumJam($jam, $menit)
{
    // Pastikan input dalam rentang yang valid
    if ($jam < 0 || $jam > 12 || $menit < 0 || $menit >= 60) {
        return "Input waktu tidak valid.";
    }

    // Hitung sudut jarum jam dan menit
    $sudutJam = 0.5 * (60 * $jam + $menit);
    $sudutMenit = 6 * $menit;

    // Hitung selisih sudut
    $selisihSudut = abs($sudutJam - $sudutMenit);

    // Pastikan selisih sudut selalu kurang dari atau sama dengan 180
    if ($selisihSudut > 180) {
        $selisihSudut = 360 - $selisihSudut;
    }

    return $selisihSudut;
}

// Input dari pengguna
echo "Masukkan jam: ";
$jam = intval(trim(fgets(STDIN)));

echo "Masukkan menit: ";
$menit = intval(trim(fgets(STDIN)));

// Contoh penggunaan
echo "Jam $jam:$menit -> " . hitungSudutJarumJam($jam, $menit) . " derajat\n";
