<?php

function generateKelipatan($n)
{
    $deret = array();

    for ($i = 1; $i <= $n; $i++) {
        $deret[] = $n * $i;
    }

    return $deret;
}

// Contoh penggunaan
$nilaiN = readline('Masukkan nilai n : ');
$deretHasil = generateKelipatan($nilaiN);

// Menampilkan hasil
echo "N = $nilaiN  -> " . implode(' ', $deretHasil) . "\n";
