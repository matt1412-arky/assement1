<?php

function fibonacci($n)
{
    if ($n == 0) return 0;
    if ($n == 1) return 1;

    $fib = [1, 1];
    for ($i = 2; $i <= $n; $i++) {
        $fib[$i] = $fib[$i - 1] + $fib[$i - 2];
    }

    return $fib;
}

function waktuMeleleh($panjang)
{
    $fibonacci = fibonacci(count($panjang) - 1);
    $lajuMeleleh = [];

    for ($i = 0; $i < count($panjang); $i++) {
        $lajuMeleleh[$i] = $panjang[$i] / $fibonacci[$i];
    }

    return $lajuMeleleh;
}

function lilinPertamaHabisMeleleh($panjang)
{
    $lajuMeleleh = waktuMeleleh($panjang);
    $minIndex = array_keys($lajuMeleleh, min($lajuMeleleh))[0];

    return $minIndex + 1;
}

$panjangLilin = [3, 3, 9, 6, 7, 8, 23];

echo "Lilin pertama yang habis meleleh adalah lilin ke-" . lilinPertamaHabisMeleleh($panjangLilin) . "\n";
