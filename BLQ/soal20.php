<?php

function rock_paper_scissor($a_play, $b_play, $d)
{
    $win = [];

    $a_play = str_split($a_play);
    $b_play = str_split($b_play);

    foreach ($a_play as $index => $a) {
        $b = $b_play[$index];

        if (($a == "G" && $b == "K") || ($a == "B" && $b == "G") || ($a == "K" && $b == "B")) {
            $win[] = "A";
            $d = $d - 2;
        } else {
            $win[] = "B";
            $d = $d + 2;
        }

        // Menangani jarak minimum yang lebih kecil dari nol
        $d = max(0, $d);

        // Menentukan pemenang jika jarak antara A dan B menjadi nol
        if ($d == 0) {
            $win = array_count_values($win);
            $a_win = $win['A'] ?? 0;
            $b_win = $win['B'] ?? 0;

            if ($a_win > $b_win) {
                return "Pemenang: Pemain A";
            } elseif ($a_win < $b_win) {
                return "Pemenang: Pemain B";
            } else {
                return "Draw";
            }
        }
    }

    return "Draw";
}

$d = 2;
$a_play = 'GGG';
$b_play = 'KKB';

echo rock_paper_scissor($a_play, $b_play, $d);
