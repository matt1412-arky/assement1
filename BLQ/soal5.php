<?php

function generateFibonacci($n)
{
    $fibonacci = array();

    for ($i = 0; $i < $n; $i++) {
        if ($i == 0) {
            $fibonacci[] = 0;
        } elseif ($i == 1) {
            $fibonacci[] = 1;
        } else {
            $fibonacci[] = $fibonacci[$i - 1] + $fibonacci[$i - 2];
        }
    }

    return $fibonacci;
}

// Meminta input dari pengguna
$n = intval(readline("Masukkan jumlah bilangan Fibonacci pertama yang ingin ditampilkan: "));

// Menampilkan n bilangan Fibonacci pertama
$bilanganFibonacci = generateFibonacci($n);

// Menampilkan hasil
echo "Bilangan Fibonacci pertama: " . implode(", ", $bilanganFibonacci) . "\n";
