<?php

function isPalindrome($kata)
{
    $kata = strtolower($kata); // Ubah menjadi huruf kecil untuk memperlakukan huruf besar dan kecil dengan sama
    $panjang = strlen($kata);

    for ($i = 0; $i < $panjang / 2; $i++) {
        if ($kata[$i] !== $kata[$panjang - $i - 1]) {
            return false;
        }
    }

    return true;
}

// Penggunaan
$input = readline("Masukkan kata: ");

if (isPalindrome($input)) {
    echo "$input adalah palindrome.\n";
} else {
    echo "$input bukan palindrome.\n";
}
