<?php

$hour_start = 18;

function drink_water($hour_start)
{
    $cal_hour = array(
        30 => 9,
        20 => 13,
        50 => 15,
        80 => 17
    );
    $hour = $hour_start - 1;
    $calories = 0;

    // Menentukan jumlah kalori
    foreach ($cal_hour as $cal => $hour_value) {
        if ($hour >= $hour_value && $cal > $calories) {
            $calories = $cal;
        }
    }

    // Menghitung durasi olahraga
    $exercise_minutes = 0.1 * $calories * $hour;

    // Menghitung konsumsi air
    $water_consumed = 0;
    while ($exercise_minutes >= 30) {
        $water_consumed += 100;
        $exercise_minutes -= 30;
    }
    $water_consumed += 500;

    // Menampilkan hasil
    echo "Donna meminum $water_consumed cc dalam $exercise_minutes menit berolahraga \n";
}

drink_water($hour_start);
