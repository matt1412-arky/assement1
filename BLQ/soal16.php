<?php
/*
Kamu dan 3 temanmu makan-makan di restoran, dan kalian memesan beberapa menu yang
nanti pembayarannya akan dibagi rata. Namun ada 1 orang yang alergi ikan, sehingga disepakati
untuk 1 jenis makanan yang mengandung ikan itu hanya 3 orang yang akan membayar. Pajak
10% dari harga makanan dan service 5% dari harga makanan. Tentukan berapa yang harus
dibayar oleh masing-masing temanmu (ingat ya ada 1 orang yang membayar lebih murah karena
alergi).
contoh: Makanan yang dipesan:
    1. Tuna Sandwich 42K (mengandung ikan)
    2. Spaghetti Carbonara 50K
    3. Tea pitcher 30K
    4. Pizza 70K
    5. Salad 30K
    6. …
*/
$jumlah_orang = readline("Masukkan Jumlah Orang: ");
$orang_yang_alergi_ikan = readline("Masukkan Jumlah Orang Yang Alergi Ikan: ");

$menu = [
    'Tuna Sandwich' => 42000,
    'Spaghetti Carbonara' => 50000,
    'Tea Pitcher' => 30000,
    'Pizza' => 70000,
    'Salad' => 30000,
    'Sushi' => 90000
];

$fish_menu =  'Tuna Sandwich';
'Sushi';

$taxed = [];
foreach ($menu as $name => $price) {
    $taxed[$name] = $price + ($price * 0.1) + ($price * 0.05);
}
$taxed_no_fish = $taxed;
unset($taxed_no_fish[$fish_menu]);
foreach ($taxed_no_fish as $name => $price) {
    $taxed_no_fish[$name] = $price / $jumlah_orang;
}
$taxed_fish_item = $taxed[$fish_menu] / ($jumlah_orang - $orang_yang_alergi_ikan);
$total = array_sum($taxed_no_fish) + $taxed_fish_item;
$total_no_fish = array_sum($taxed_no_fish);
echo "Total yang harus dibayar oleh orang yang alergi ikan: " . number_format($total_no_fish, 2) . " IDR\n";
echo "Total yang harus dibayar oleh masing-masing teman (tidak alergi ikan) : " . number_format($total, 2) . " IDR\n";
