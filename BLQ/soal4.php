<?php

function generatePrimes($n)
{
    $primes = array();
    $candidate = 2;

    while (count($primes) < $n) {
        if (isPrime($candidate)) {
            $primes[] = $candidate;
        }
        $candidate++;
    }

    return $primes;
}

function isPrime($num)
{
    if ($num < 2) {
        return false;
    }

    for ($i = 2; $i <= sqrt($num); $i++) {
        if ($num % $i == 0) {
            return false;
        }
    }

    return true;
}

// Meminta input dari pengguna
$n = intval(readline("Masukkan jumlah bilangan prima pertama yang ingin ditampilkan: "));

// Menampilkan n bilangan prima pertama
$bilanganPrima = generatePrimes($n);

// Menampilkan hasil
echo "Bilangan prima pertama: " . implode(", ", $bilanganPrima) . "\n";
