<?php

function findMinMaxSum($deret)
{
    $minSum = PHP_INT_MAX;
    $maxSum = PHP_INT_MIN;

    $jumlahAngka = count($deret);

    if ($jumlahAngka < 4) {
        return array("error" => "Deret tidak cukup panjang untuk mencari 4 komponen.");
    }

    // Mengiterasi semua kombinasi 4 angka
    for ($i = 0; $i <= $jumlahAngka - 4; $i++) {
        for ($j = $i + 1; $j <= $jumlahAngka - 3; $j++) {
            for ($k = $j + 1; $k <= $jumlahAngka - 2; $k++) {
                for ($l = $k + 1; $l <= $jumlahAngka - 1; $l++) {
                    $sum = $deret[$i] + $deret[$j] + $deret[$k] + $deret[$l];
                    $minSum = min($minSum, $sum);
                    $maxSum = max($maxSum, $sum);
                }
            }
        }
    }

    return array("minSum" => $minSum, "maxSum" => $maxSum);
}

// Input deret angka dari pengguna
$input = readline("Masukkan deret angka (pisahkan dengan spasi): ");
$deret = array_map('intval', explode(' ', $input));

// Mencari nilai minimal dan maksimal
$result = findMinMaxSum($deret);

// Menampilkan hasil
if (isset($result["error"])) {
    echo "Error: " . $result["error"] . "\n";
} else {
    echo "Nilai Minimal Penjumlahan 4 Komponen: " . $result["minSum"] . "\n";
    echo "Nilai Maksimal Penjumlahan 4 Komponen: " . $result["maxSum"] . "\n";
}
