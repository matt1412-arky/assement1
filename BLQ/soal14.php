<?php

function rotasiKiriDeret($deret, $n)
{
    $n = $n % count($deret); // Menghindari rotasi lebih dari panjang deret

    if ($n == 1) {
        // Jika N = 1, pindahkan angka pertama ke belakang
        $angkaPertama = array_shift($deret);
        array_push($deret, $angkaPertama);

        // Ganti angka yang terkena rotasi dengan nilai N
        $deret[count($deret) - 1] = $n;
    } else {
        // Rotasi elemen dalam deret
        $rotated = array_merge(array_slice($deret, $n), array_slice($deret, 0, $n));
        $deret = $rotated;
    }

    return $deret;
}

// Input nilai N dari pengguna
$n = readline("Masukkan nilai N: ");

// Contoh penggunaan
$deret = [3, 9, 0, 7, 1, 2, 4];
$rotatedResult = rotasiKiriDeret($deret, $n);
echo "Deret : " . implode(' ', $deret) . "\n";
echo "N = $n " . "-> " . implode(' ', $rotatedResult) . "\n";
