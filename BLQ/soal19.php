<?php
function isPangram($kalimat)
{
    // Mengubah kalimat menjadi huruf kecil agar tidak terpengaruh oleh huruf besar atau kecil
    $kalimat = strtolower($kalimat);

    // Menghilangkan karakter selain huruf
    $kalimat = preg_replace("/[^a-z]/", '', $kalimat);

    // Mengubah string menjadi array karakter unik
    $karakterUnik = array_unique(str_split($kalimat));

    // Jika jumlah karakter unik adalah 26 (jumlah total huruf dalam alfabet), itu adalah pangram
    return count($karakterUnik) == 26;
}

// Kalimat-kalimat yang akan diuji
$kalimat1 = "Sphinx of black quartz, judge my vow";
$kalimat2 = "Brawny gods just flocked up to quiz and vex him";
$kalimat3 = "Check back tomorrow; I will see if the book has arrived.";
// $kalimat4 = "A quick brown fox jumps over the lazy dog";

// Menentukan apakah kalimat-kalimat tersebut adalah pangram atau bukan
echo "Kalimat 1: " . (isPangram($kalimat1) ? "Pangram" : "Bukan Pangram") . "\n";
echo "Kalimat 2: " . (isPangram($kalimat2) ? "Pangram" : "Bukan Pangram") . "\n";
echo "Kalimat 3: " . (isPangram($kalimat3) ? "Pangram" : "Bukan Pangram") . "\n";
// echo "Kalimat 4: " . (isPangram($kalimat4) ? "Pangram" : "Bukan Pangram") . "\n";
